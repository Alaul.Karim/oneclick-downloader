# Farewell!

### It was an awesome experience developing this extension, but now it is time to say goodbye! I thank everyone who suggested hosters and helped by reporting bugs and providing download functions.

Times change and so do I. And my focus shifts towards other things. Since some time I am not able (and to be honest: not that much involved in it) to fix broken hosters.

So this extension’s development has officially ended. The extension will stay online in the store and here on GitLab as long as I feel like and as long as Google does not delete it.

Have an awesome time!

Thank you!
