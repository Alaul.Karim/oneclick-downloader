function replaceVariables(scheme, source, linkObject) {
    var strName = source.split('.').slice(0, -1).join('.');
    var strSuffix = source.split('.').pop();
    var result = scheme

    // Replace image and tab specific variables
    result = result.replace(/%full%/g, source);
    result = result.replace(/%name%/g, strName);
    result = result.replace(/%suffix%/g, strSuffix);
    result = result.replace(/%order%/g, linkObject.dataset.order);
    result = result.replace(/%tabID%/g, linkObject.dataset.tabid);
    result = result.replace(/%tabTitle%/g, linkObject.dataset.tabtitle);

    // Date
    var dateObject = new Date()
    var date = {
        day: dateObject.getDate().toString().padStart(2, '0'),
        month: (dateObject.getMonth()+1).toString().padStart(2, '0'),
        year: dateObject.getFullYear().toString()
    };
    result = result.replace(/%day%/g, date.day);
    result = result.replace(/%month%/g, date.month);
    result = result.replace(/%year%/g, date.year);
    result = result.replace(/%isodate%/g,date.year+'-'+date.month+'-'+date.day);

    // Sanitize
    result = result.replace(/[\/:"?~<>*|]/g, '-');

    return result;
}


function appendVariablesHelp(targetElementId) {
    var target = document.getElementById(targetElementId);
    var table = document.createElement('table');
    var thead = document.createElement('thead');
    var theadTR = document.createElement('tr');
    var tbody = document.createElement('tbody');
    var dateObject = new Date();
    var inParagraph = document.createElement('p');
    var inText = document.createTextNode(t('variablesHelpIntroduction', true));

    var examples = {
        full: t('variablesHelpVarFullImageName', true) + '.jpg',
        name: t('variablesHelpVarFullImageName', true),
        suffix: 'jpg',
        order: (Math.floor(Math.random() * 10) + 1).toString().padStart(2, '0'),
        tabID: Math.floor(Math.random() * 99) + 11,
        tabTitle: t('variablesHelpVarTabTitleExample', true),
        year: dateObject.getFullYear().toString(),
        month: (dateObject.getMonth()+1).toString().padStart(2, '0'),
        day: dateObject.getDate().toString().padStart(2, '0')
    };
    examples['isodate'] = [
        examples['year'],
        examples['month'],
        examples['day']].join('-');



    var variables = {
        full: t('variablesHelpVarFull', true),
        name: t('variablesHelpVarName', true),
        suffix: t('variablesHelpVarSuffix', true),
        order: t('variablesHelpVarOrder', true),
        tabID: t('variablesHelpVarTabId', true),
        tabTitle: t('variablesHelpVarTabTitle', true),
        year: t('variablesHelpVarYear', true),
        month: t('variablesHelpVarMonth', true),
        day: t('variablesHelpVarDay', true),
        isodate: t('variablesHelpIsodate', true)
    };

    var headings = {
        variablesHelpTheadVariable: t('variablesHelpTableExample', true),
        variablesHelpTheadDescription: t('variablesHelpTableDescription', true),
        variablesHelpTheadExample: t('variablesHelpTableExample', true)
    }

    for (var heading in headings) {
        var th = document.createElement('th');
        var text = document.createTextNode(headings[heading]);
        th.appendChild(text);
        theadTR.appendChild(th);
    }

    for (var variable in variables) {
        var tr = document.createElement('tr');
        var tdVariable = document.createElement('td');
        var tdDescription = document.createElement('td');
        var tdExample = document.createElement('td');
        var code = document.createElement('code');
        var variableExample = document.createTextNode('%'+variable+'%');
        var description = document.createTextNode(variables[variable]);
        var example = document.createTextNode(examples[variable]);
        code.appendChild(variableExample);
        tdVariable.appendChild(code);
        tdDescription.appendChild(description);
        tdExample.appendChild(example);
        tr.appendChild(tdVariable);
        tr.appendChild(tdDescription);
        tr.appendChild(tdExample);
        tbody.appendChild(tr);
    }

    target.innerHTML = '';
    inParagraph.appendChild(inText);
    target.appendChild(inParagraph);
    thead.appendChild(theadTR);
    table.appendChild(thead);
    table.appendChild(tbody);
    target.appendChild(table);
}
