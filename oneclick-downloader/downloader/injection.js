chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

    if (request.action == 'getLinks') {

        var links = document.getElementsByTagName('a');
        var hosters = request.hosters;
        var regexArray = [];
        var resultLinks = [];
        var order = 0;

        for (var i=0; i < hosters.length; i++) {
            regexArray.push(hosters[i]['pattern']);
        }

        for (var i=0; i < links.length; i++) {
            var href = links[i].href;
            for (var j=0; j < regexArray.length; j++) {
                var regex = new RegExp(regexArray[j], 'i');
                if (regex.test(href) === true) {
                    order++;
                    resultLinks.push({
                        order: order,
                        href: href,
                        btoa: this.btoa(links[i].outerHTML)
                    });
                }
            }
        }

        resultLinks = resultLinks.filter(function(value, index, self) {
            return self.indexOf(value) === index;
        });

        sendResponse({
            links: resultLinks,
            tabID: request.tabID,
            tabTitle: document.title
        });

    }

});
