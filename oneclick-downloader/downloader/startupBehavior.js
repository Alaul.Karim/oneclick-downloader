function setStartupBehavior() {
    // Since chrome.storage.sync is forcefully asynchronous this nasty and
    // disgusting hack is used to store the startup behavior as attribute to
    // the html tag in the DOM which for itself is HTML5 compliant because it
    // uses the data- prefix for the attribute. It’s still nasty)
    chrome.storage.sync.get(
        {'startupBehavior': 'default'},
        function(o) {
            var html = document.getElementsByTagName('html')[0];
            html.setAttribute('data-startup-behavior', o.startupBehavior);
        });
}

function getStartupBehavior() {
    var html = document.getElementsByTagName('html')[0];
    return html.getAttribute('data-startup-behavior');
}
