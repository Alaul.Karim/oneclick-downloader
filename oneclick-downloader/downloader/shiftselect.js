function shiftSelect(obj) {
    var lastChecked = null;
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    var dataAttribute = 'data-shift-select-index'

    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].setAttribute(dataAttribute, i);
    }

    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].addEventListener('click', function(e) {
            // Get lastChecked object and set the target state
            if(!lastChecked || !e.shiftKey) { lastChecked = this; }
            var set_to = this.checked;

            // Set lowest and highest ID and bring in correct order
            var i = parseInt(lastChecked.getAttribute(dataAttribute));
            var j = parseInt(this.getAttribute(dataAttribute));
            var low = i; var high = j;
            if (i > j){ var low = j; var high = i; }

            // Set the target state to all checkboxes between the lowest and
            // the highest ID that were selected
            for(var current = 0; current < checkboxes.length; current++) {
                if (low <= current && current <= high) {
                    checkboxes[current].checked = set_to;
                }
            }
        });
    }
}
